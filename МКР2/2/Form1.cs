﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;


namespace МКР2_2_
{
    public partial class Form1 : Form
    {
        private DataTable autoTable;
        

        public Form1()
        {
            InitializeComponent();
            autoTable = new DataTable("kiosk");


            autoTable.Columns.Add("ПІБ", typeof(string));
            autoTable.Columns.Add("Марка", typeof(string));
            autoTable.Columns.Add("ціна",typeof(int));
            autoTable.Columns.Add("Номери", typeof(int));
            autoTable.Columns.Add("Aреса", typeof(string));

            autoTable.Rows.Add("Гринців", "Ford", 1000,8976,"ЛЬВІВ" );
            autoTable.Rows.Add("Білокур", "Ford", 2000,9807,"Київ" );
            autoTable.Rows.Add("Дудич", "Audi", 2000,6484,"Ужгород" );
            autoTable.Rows.Add("Луців", "BMW", 3000, 7894,"Київ");
            autoTable.Rows.Add("Іванусів", "BMW", 2000,8974,"Миколаїв" );



            dataGridView1.DataSource = autoTable;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string selectedCarMake = "X"; // Задайте бажану марку автомобіля
            Filter(selectedCarMake);
        }

        private void Filter(string carMake)
        {
            int count = 0; // Змінна для збереження кількості власників

            foreach (DataRow row in autoTable.Rows)
            {
                if (row["Марка"].ToString() == carMake && row["Номери"].ToString().Contains("7"))
                {
                    count++;
                }
            }

            MessageBox.Show($"Кількість власників машини марки {carMake}, у номері яких є принаймні одна цифра 7: {count}");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            int totalSum = 0;

            foreach (DataRow row in autoTable.Rows)
            {
                int carPrice = Convert.ToInt32(row["ціна"]);
                totalSum += carPrice;
            }

            MessageBox.Show($"Загальна сума автомобілів: {totalSum}");
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("auto_data.txt"))
            {
                foreach (DataRow row in autoTable.Rows)
                {
                    string line = $"{row["ПІБ"]},{row["Марка"]},{row["ціна"]},{row["Номери"]},{row["Aреса"]}";
                    writer.WriteLine(line);
                }
            }

            MessageBox.Show("Дані успішно збережено в файлі 'auto_data.txt'.");
        }
    }
    
}
    

