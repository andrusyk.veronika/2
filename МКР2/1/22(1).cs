﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace дфи22._1App19
{
    public partial class Form1 : Form
    {
        int[] mas;
        int n;
        Random ran = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridViewMas.RowCount = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int max = int.MinValue;

            for (int i = 0; i < dataGridViewMas.Rows.Count; i++)
            {
                if (i % 2 == 0)
                {
                    int currentValue = Convert.ToInt32(dataGridViewMas.Rows[i].Cells[0].Value);
                    if (currentValue > max)
                    {
                        max = currentValue;
                    }
                }
            }

            MessageBox.Show("Найбільший елемент серед елементів вектора з парними індексами: " + max);
       
        }

        
    }
}
